FROM eclipse-temurin:11-alpine

RUN apk update \
    && apk add busybox-extras \
    && mkdir /archivos \
    && apk add tzdata \
    && cp /usr/share/zoneinfo/America/Bogota /etc/localtime \
    && echo 'America/Bogota' > /etc/timezone 
    
VOLUME /archivos 
    
ARG JAR_VERSION 
ENV MIN_MEMORY=128
ENV MAX_MEMORY=256

COPY target/test-api-itc-2022-${JAR_VERSION}.jar /app.jar

EXPOSE $API_PORT

ENTRYPOINT [ "sh", "-c", "java -jar -Djava.security.egd=file:/dev/./urandom -Xmx${MAX_MEMORY}m -Xms${MIN_MEMORY}m -Dserver.port=$API_PORT /app.jar" ]

