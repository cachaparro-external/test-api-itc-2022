package com.itc.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestApiItc2022Application {

	public static void main(String[] args) {
		SpringApplication.run(TestApiItc2022Application.class, args);
	}

}
