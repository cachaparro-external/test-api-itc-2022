package com.itc.test.dto;

import java.time.LocalDateTime;

public class GreetingResponseDTO {

	private String greeting;
	private LocalDateTime greetingDate;
	
	public String getGreeting() {
		return greeting;
	}
	
	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}
	
	public LocalDateTime getGreetingDate() {
		return greetingDate;
	}
	
	public void setGreetingDate(LocalDateTime greetingDate) {
		this.greetingDate = greetingDate;
	}
}
