package com.itc.test.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itc.test.dto.GreetingRequestDTO;
import com.itc.test.dto.GreetingResponseDTO;

@RestController
@RequestMapping("/api/greeting")
public class GreetingApi {
	
	private final String MAIN_PATH = "/home/saenz-laptop/tmp/java/";
	//private final String MAIN_PATH = "/archivos/";
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/name/{name}")
	public GreetingResponseDTO greeting(
			@PathVariable(name = "name") String name) {
		GreetingResponseDTO dto = new GreetingResponseDTO();
		dto.setGreeting("Hello " + name);
		dto.setGreetingDate(LocalDateTime.now());
		
		return dto;
	}
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/bye")
	public GreetingResponseDTO bye(
			@RequestBody GreetingRequestDTO request) {
		GreetingResponseDTO dto = new GreetingResponseDTO();
		dto.setGreeting("Bye " + request.getName());
		dto.setGreetingDate(LocalDateTime.now());
		
		return dto;
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/file/{filename}")
	public List<GreetingResponseDTO> masiveGreeting(
			@PathVariable(name="filename") String filename) throws FileNotFoundException{
			
		List<GreetingResponseDTO> saludos = new ArrayList<>();
		
		File file = null;
		Reader r = null;
		BufferedReader br = null;
		
		try {
			file = new File(this.MAIN_PATH + filename);	
			
			if(!file.exists()) {
				throw new FileNotFoundException("Archivo " + file.getAbsolutePath() + " no existe.");
			}
			
			r = new FileReader(file);
			
			br = new BufferedReader(r);
			
			br.lines().forEach((line) -> {
				GreetingResponseDTO response = new GreetingResponseDTO();
				response.setGreeting("Hello " + line);
				response.setGreetingDate(LocalDateTime.now());
				
				saludos.add(response);
			});
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
			throw e;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(r != null) {
				try {
					r.close();
				} catch (IOException e) {					
					e.printStackTrace();
				}
			}
		}
		
		return saludos;
		
	}

}
